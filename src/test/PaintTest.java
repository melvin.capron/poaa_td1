package test;

import drawing.commands.CommandHistory;
import drawing.commands.UngroupCommand;
import drawing.core.PaintApplication;
import drawing.shapes.adapter.ShapeAdapter;
import drawing.shapes.adapter.ShapeGroup;
import drawing.shapes.interfaces.IShape;
import drawing.status.ButtonFactory;
import drawing.status.StatusBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.sql.BatchUpdateException;
import java.util.Iterator;

import static org.junit.Assert.*;

public class PaintTest extends ApplicationTest {

    PaintApplication app;

    @Override
    public void start(Stage stage) {
        try {
            app = new PaintApplication();
            app.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_draw_circle_programmatically() {
        interact(() -> {
            app.getDrawingPane().addShape(new ShapeAdapter(new Ellipse(20, 20, 30, 30)));
        });
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Ellipse);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_circle() {
        // given:
        clickOn(ButtonFactory.CIRCLE_BUTTON);
        moveBy(60,60);

        // when:
        drag().dropBy(30,30);
        //press(MouseButton.PRIMARY); moveBy(30,30); release(MouseButton.PRIMARY);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Ellipse);
        assertFalse(it.hasNext());

    }

    @Test
    public void should_draw_rectangle() {
        // given:
        clickOn(ButtonFactory.RECTANGLE_BUTTON);
        moveBy(0, 60);

        // when:
        drag().dropBy(70, 40);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Rectangle);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_triangle() {
        // given:
        clickOn(ButtonFactory.TRIANGLE_BUTTON);
        moveBy(0, 60);

        // when:
        drag().dropBy(50, 80);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Polygon);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_clear() {
        // given:
        clickOn(ButtonFactory.RECTANGLE_BUTTON);
        moveBy(30, 60).drag().dropBy(70, 40);
        clickOn(ButtonFactory.CIRCLE_BUTTON);
        moveBy(-30, 160).drag().dropBy(70, 40);

        // when:
        clickOn(ButtonFactory.CLEAR_BUTTON);

        // then:
        assertFalse(app.getDrawingPane().iterator().hasNext());
    }

    @Test
    public void testSelectedSize() {
        IShape shape = new ShapeAdapter(new Ellipse(20, 20, 30, 30));
        interact(() -> {
            app.getDrawingPane().addShape(shape);
        });

        interact(() -> {
            app.getDrawingPane().getSelectionHandler().addToSelection(shape);
        });

        assertEquals(1, app.getDrawingPane().getSelection().size());
    }

    @Test
    public void testDeleteForm() {
        IShape shape = new ShapeAdapter(new Ellipse(20, 20, 30, 30));
        interact(() -> {
            app.getDrawingPane().addShape(shape);
            app.getDrawingPane().getSelectionHandler().addToSelection(shape);
            app.getDrawingPane().getSelectionHandler().cleanSelectedShapes();
            app.getDrawingPane().removeShape(shape);
        });

        assertEquals(0, app.getDrawingPane().getSelection().size());
        assertFalse(app.getDrawingPane().iterator().hasNext());
    }

    @Test
    public void should_group() {
        // given:
        IShape shape1 = new ShapeAdapter(new Ellipse(20, 20, 30, 30));
        IShape shape2 = new ShapeAdapter(new Rectangle(20, 20, 30, 30));

        ShapeGroup sg = new ShapeGroup();

        interact(() -> {
            sg.add(shape1);
            sg.add(shape2);

            app.getDrawingPane().addShape(sg);
            app.getDrawingPane().getSelectionHandler().addToSelection(sg);
        });

        assertEquals(1, app.getDrawingPane().getSelection().size()); //1 group
        assertEquals(2, sg.getShapes().size()); //2 shapes in this group
    }

    @Test
    public void should_ungroup() {
        // given:
        IShape shape1 = new ShapeAdapter(new Ellipse(20, 20, 30, 30));
        IShape shape2 = new ShapeAdapter(new Rectangle(20, 20, 30, 30));

        ShapeGroup sg = new ShapeGroup();

        interact(() -> {
            sg.add(shape1);
            sg.add(shape2);

            app.getDrawingPane().addShape(sg);
            app.getDrawingPane().getSelectionHandler().addToSelection(sg);
        });

        assertEquals(1, app.getDrawingPane().getSelection().size()); //1 group
        assertEquals(2, sg.getShapes().size()); //2 shapes in this group

        // ungroup

        interact(() -> {
            app.getDrawingPane().getCommandHistory().exec(new UngroupCommand(app.getDrawingPane()));
        });

        assertTrue(app.getDrawingPane().getIterator().hasNext());
        app.getDrawingPane().getIterator().next();
        assertTrue(app.getDrawingPane().getIterator().hasNext());
        app.getDrawingPane().getIterator().next();
        assertTrue(app.getDrawingPane().getIterator().hasNext());
    }
}