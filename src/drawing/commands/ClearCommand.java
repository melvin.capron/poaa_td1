package drawing.commands;

import drawing.core.DrawingPane;
import drawing.observer.EventManager;
import drawing.shapes.interfaces.IShape;

import java.util.ArrayList;
import java.util.Iterator;

public class ClearCommand implements ICommand {
    private final DrawingPane drawingPane;
    private ArrayList<IShape> cleanedShapes;
    private EventManager eventManager;

    /**
     *
     * @param drawingPane to act on
     * @param eventManager to alert
     */
    public ClearCommand(DrawingPane drawingPane, EventManager eventManager){
        this.drawingPane = drawingPane;
        this.eventManager = eventManager;
    }

    @Override
    public void execute() {
        this.cleanedShapes = new ArrayList<>();

        for (Iterator<IShape> it = this.drawingPane.getIterator(); it.hasNext(); ) {
            IShape shape = it.next();
            this.cleanedShapes.add(shape);
        }

        for(IShape shape : cleanedShapes){
            drawingPane.removeShape(shape);
        }

        eventManager.notifyListeners("cleanShape");

        this.drawingPane.getSelectionHandler().cleanSelectedShapes();
    }

    @Override
    public void undo() {
        for(IShape shape : cleanedShapes){
            this.drawingPane.addShape(shape);
        }
    }

    @Override
    public void redo(){
        for(IShape shape : cleanedShapes){
            drawingPane.removeShape(shape);
        }

        eventManager.notifyListeners("cleanShape");

        this.drawingPane.getSelectionHandler().cleanSelectedShapes();
    }
}
