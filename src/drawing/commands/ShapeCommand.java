package drawing.commands;

import drawing.core.DrawingPane;
import drawing.handlers.ShapeButtonHandler;
import drawing.shapes.AbstractShape;
import drawing.shapes.interfaces.IShape;

public class ShapeCommand implements ICommand {
    private final DrawingPane drawingPane;
    private IShape createdShape;
    private AbstractShape creatorShape;

    /**
     *
     * @param drawingPane to act on
     */
    public ShapeCommand(DrawingPane drawingPane, AbstractShape creatorShape){
        this.drawingPane = drawingPane;
        this.creatorShape = creatorShape;
    }

    @Override
    public void execute(){
        IShape newShape = creatorShape.createShape();
        this.drawingPane.addShape(newShape);
        this.createdShape = newShape;
    }

    public void populateCoords(double originX, double originY, double destinationX, double destinationY){
        this.creatorShape.populateCoords(originX, originY, destinationX, destinationY);
    }

    @Override
    public void undo() {
        this.drawingPane.removeShape(createdShape);
    }

    @Override
    public void redo() {
        this.drawingPane.addShape(createdShape);
    }
}
