package drawing.commands;

import drawing.core.DrawingPane;
import drawing.observer.EventManager;
import drawing.shapes.interfaces.IShape;

import java.util.ArrayList;

public class RemoveCommand implements ICommand {
    private final DrawingPane drawingPane;
    private ArrayList<IShape> removedShapes;

    /**
     *
     * @param drawingPane to use
     */
    public RemoveCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        this.removedShapes = new ArrayList<>();
        ArrayList<IShape> removed = new ArrayList<>();

        for (IShape selected : this.drawingPane.getSelection()) {
            this.drawingPane.removeShape(selected);
            removed.add(selected);
            this.removedShapes.add(selected);
        }

        this.drawingPane.getSelectionHandler().removeFromSelection(removed);
    }

    @Override
    public void undo() {
        for(IShape shape : this.removedShapes){
            this.drawingPane.addShape(shape);
        }
    }

    @Override
    public void redo(){
        ArrayList<IShape> removed = new ArrayList<>();

        for (IShape selected : this.removedShapes) {
            this.drawingPane.removeShape(selected);
            removed.add(selected);
            this.removedShapes.add(selected);
        }

        this.drawingPane.getSelectionHandler().removeFromSelection(removed);
    }
}
