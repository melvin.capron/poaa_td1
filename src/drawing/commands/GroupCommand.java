package drawing.commands;

import drawing.core.DrawingPane;
import drawing.shapes.adapter.ShapeGroup;
import drawing.shapes.interfaces.IShape;

import java.util.ArrayList;

public class GroupCommand implements ICommand {
    private final DrawingPane drawingPane;
    private ArrayList<IShape> groupedShapes;
    private ShapeGroup groupAdded = null;

    /**
     *
     * @param drawingPane to use
     */
    public GroupCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        ShapeGroup group = new ShapeGroup();
        this.groupedShapes = new ArrayList<>();

        for (IShape shape : this.drawingPane.getSelection()) {
            this.groupedShapes.add(shape);
            this.drawingPane.removeShape(shape);
            group.add(shape);
            shape.setSelected(false);
        }

        this.drawingPane.addShape(group);
        this.groupAdded = group;
    }

    @Override
    public void undo() {
        this.drawingPane.removeShape(groupAdded);

        for(IShape shape : this.groupedShapes){
            this.drawingPane.addShape(shape);
        }
    }

    @Override
    public void redo() {
        this.drawingPane.addShape(groupAdded);

        for(IShape shape : this.groupedShapes){
            this.drawingPane.removeShape(shape);
        }
    }
}
