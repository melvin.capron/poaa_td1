package drawing.commands;

import drawing.core.DrawingPane;
import drawing.shapes.interfaces.IShape;
import javafx.scene.shape.Shape;

import java.util.ArrayList;

public class MouseMoveCommand implements ICommand {
    private final DrawingPane drawingPane;
    private ArrayList<IShape> selectedShapes;

    /**
     *
     * @param drawingPane to act on
     */
    public MouseMoveCommand(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        this.selectedShapes = this.drawingPane.getSelection();
    }

    @Override
    public void undo() {
        for(IShape shape : selectedShapes){
            shape.offset(-shape.getX(), -shape.getY());
        }
    }

    public void redo(){
        for(IShape shape : selectedShapes){
            shape.offset(shape.getX(), shape.getY());
        }
    }
}
