package drawing.commands;

import drawing.core.DrawingPane;
import drawing.shapes.adapter.ShapeGroup;
import drawing.shapes.interfaces.IShape;

import java.util.ArrayList;

public class UngroupCommand implements ICommand {
    private final DrawingPane drawingPane;
    private ArrayList<IShape> groupedShapes;
    private ShapeGroup groupAdded = null;

    /**
     * @param drawingPane to use
     */
    public UngroupCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        this.groupedShapes = new ArrayList<>();

        for (IShape group : this.drawingPane.getSelection()) {
            if (group instanceof ShapeGroup) {
                this.drawingPane.removeShape(group);
                for (IShape shape : ((ShapeGroup) group).getShapes()) {
                    this.drawingPane.addShape(shape);
                    shape.setSelected(false);
                }
            }
        }
    }

    @Override
    public void undo() {
        for (IShape shape : this.groupedShapes) {
            this.drawingPane.removeShape(shape);
        }

        this.drawingPane.addShape(groupAdded);
    }

    @Override
    public void redo() {
        for (IShape shape : this.groupedShapes) {
            this.drawingPane.addShape(shape);
        }

        this.drawingPane.removeShape(groupAdded);
    }
}
