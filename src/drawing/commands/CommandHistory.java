package drawing.commands;

import java.util.ArrayList;

public class CommandHistory {
    public final ArrayList<ICommand> undoCommands = new ArrayList<>();
    public final ArrayList<ICommand> redoComands = new ArrayList<>();

    public void exec(ICommand command){
        undoCommands.add(command);
        command.execute();
    }

    public void undo(){
        if(undoCommands.size() > 0){
            ICommand lastCommand = getLastCommand(undoCommands);
            lastCommand.undo();
            undoCommands.remove(lastCommand);
            redoComands.add(lastCommand);
        }
    }

    public void redo(){
        if(redoComands.size() > 0){
            ICommand lastCommand = getLastCommand(redoComands);
            lastCommand.redo();
            redoComands.remove(lastCommand);
            undoCommands.add(lastCommand);
        }
    }

    private ICommand getLastCommand(ArrayList<ICommand> commands){
        return commands.get(undoCommands.size() - 1);
    }
}
