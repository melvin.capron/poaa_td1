package drawing.observer;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {
    private HashMap<String, ArrayList<FxObserverInterface>> listeners;

    /**
     * Subscribe to observer
     * @param event to listen
     * @param listener associated
     */
    public void subscribe(String event, FxObserverInterface listener) {
        if(this.listeners == null){
            this.listeners = new HashMap<>();
        }

        if (!this.listeners.containsKey(event)) {
            this.listeners.put(event, new ArrayList<>());
        }

        this.listeners.get(event).add(listener);

    }

    /**
     * Unsubscribe from observer
     * @param event to listen
     * @param listener associated
     */
    public void unsubscribe(String event, FxObserverInterface listener) {
        this.listeners.get(event).remove(listener);
    }

    /**
     * Notify all related listeners
     * @param event to notify
     */
    public void notifyListeners(String event) {
        for (FxObserverInterface listener : listeners.get(event)) {
            listener.update(event);
        }
    }
}
