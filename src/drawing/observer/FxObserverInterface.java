package drawing.observer;

public interface FxObserverInterface {
    void update(String event);
}
