package drawing.handlers;

import drawing.commands.ICommand;
import drawing.commands.ShapeCommand;
import drawing.core.DrawingPane;
import drawing.shapes.AbstractShape;
import drawing.shapes.interfaces.IShape;
import drawing.shapes.interfaces.ShapeCreator;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class ShapeButtonHandler implements EventHandler<Event> {

    private DrawingPane drawingPane;
    private ShapeCommand command;
    protected double originX;
    protected double originY;
    protected double destinationX;
    protected double destinationY;

    protected IShape shape;

    public ShapeButtonHandler(DrawingPane drawingPane, ShapeCommand command) {
        this.drawingPane = drawingPane;
        this.command = command;
    }

    @Override
    public void handle(Event event) {

        if (event instanceof ActionEvent) {
            drawingPane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        }

        if (event instanceof MouseEvent) {
            if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                drawingPane.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
                originX = ((MouseEvent) event).getX();
                originY = ((MouseEvent) event).getY();
            }

            if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                destinationX = ((MouseEvent) event).getX();
                destinationY = ((MouseEvent) event).getY();
                command.populateCoords(originX, originY, destinationX, destinationY);
                this.drawingPane.getCommandHistory().exec(command);

                drawingPane.removeEventHandler(MouseEvent.MOUSE_PRESSED, this);
                drawingPane.removeEventHandler(MouseEvent.MOUSE_RELEASED, this);
            }
        }
    }
}
