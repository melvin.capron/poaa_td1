package drawing.handlers;

import drawing.core.DrawingPane;
import drawing.shapes.interfaces.IShape;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import drawing.observer.EventManager;

import java.util.ArrayList;
import java.util.Iterator;

public class SelectionHandler implements EventHandler<MouseEvent> {

    private DrawingPane drawingPane;

    public ArrayList<IShape> getSelectedShapes() {
        return selectedShapes;
    }

    private ArrayList<IShape> selectedShapes = new ArrayList<>();

    public SelectionHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        drawingPane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
    }

    @Override
    public void handle(MouseEvent event) {
        if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
            boolean clicked = false;

            for (Iterator<IShape> it = drawingPane.getIterator(); it.hasNext(); ) {
                IShape shape = it.next();
                if (shape.isOn(event.getX(), event.getY())) {
                    clicked = true;

                    if (!event.isShiftDown()) {
                        cleanSelectedShapes();
                    }

                    if (!shape.isSelected()) {
                        this.addToSelection(shape);
                    } else {
                        this.removeOne(shape);
                    }

                    shape.setSelected(!shape.isSelected());

                    break;
                }
            }

            if (!clicked) {
                this.cleanSelectedShapes();
            }
        }
    }

    /**
     * Clean all selected shapes
     */
    public void cleanSelectedShapes() {
        for (IShape shape : selectedShapes) {
            shape.setSelected(false);
        }

        this.drawingPane.getEventManager().notifyListeners("cleanSelectedShape");
        selectedShapes.clear();
    }

    /**
     * @param shape to add
     */
    public void addToSelection(IShape shape) {
        selectedShapes.add(shape);
        this.drawingPane.getEventManager().notifyListeners("addSelectedShape");
    }

    /**
     * @param shape to remove
     */
    public void removeOne(IShape shape) {
        shape.setSelected(false);
        this.drawingPane.getEventManager().notifyListeners("removeSelectedShape");
        selectedShapes.remove(shape);
    }

    /**
     * @param shapes all selected to remove
     */
    public void removeFromSelection(ArrayList<IShape> shapes) {
        for (IShape shape : shapes) {
            this.removeOne(shape);
        }
    }
}
