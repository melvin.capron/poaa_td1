package drawing.handlers;

import drawing.commands.ICommand;
import drawing.core.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    private ICommand command;

    public ButtonHandler(DrawingPane drawingPane, ICommand command) {
        this.drawingPane = drawingPane;
        this.command = command;
    }

    @Override
    public void handle(ActionEvent event) {
        this.drawingPane.getCommandHistory().exec(command);
    }
}
