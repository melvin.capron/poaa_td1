package drawing.handlers;

import drawing.core.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RedoButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;

    public RedoButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event) {
        this.drawingPane.getCommandHistory().redo();
    }
}
