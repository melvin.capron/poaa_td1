package drawing.core;

import drawing.commands.CommandHistory;
import drawing.commands.MouseMoveCommand;
import drawing.shapes.interfaces.IShape;
import drawing.handlers.MouseMoveHandler;
import drawing.handlers.SelectionHandler;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;
import drawing.observer.EventManager;

import java.util.ArrayList;
import java.util.Iterator;

public class DrawingPane extends Pane implements Iterable<IShape> {
    private MouseMoveHandler mouseMoveHandler;

    private SelectionHandler selectionHandler;

    private ArrayList<IShape> shapes;

    private EventManager eventManager;

    private CommandHistory commandHistory;

    /**
     * DrawingPane constructor
     */
    public DrawingPane(EventManager eventManager, CommandHistory commandHistory) {
        this.commandHistory = commandHistory;
        clipChildren();
        shapes = new ArrayList<>();
        this.eventManager = eventManager;
        this.selectionHandler = new SelectionHandler(this);
        this.mouseMoveHandler = new MouseMoveHandler(this, new MouseMoveCommand(this));
        this.commandHistory = commandHistory;
    }

    /**
     * @return the EventManager
     */
    public EventManager getEventManager() {
        return eventManager;
    }

    /**
     * @return the SelectionHandler
     */
    public SelectionHandler getSelectionHandler() {
        return selectionHandler;
    }

    /**
     * Clips the children of this {@link Region} to its current size.
     * This requires attaching a change listener to the region’s layout bounds,
     * as JavaFX does not currently provide any built-in way to clip children.
     */
    void clipChildren() {
        final Rectangle outputClip = new Rectangle();
        this.setClip(outputClip);

        this.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
            outputClip.setWidth(newValue.getWidth());
            outputClip.setHeight(newValue.getHeight());
        });
    }

    /**
     * Add shape to pane
     *
     * @param shape to add
     */
    public void addShape(IShape shape) {
        shapes.add(shape);
        shape.addShapeToPane(this);
        eventManager.notifyListeners("addShape");
    }

    /**
     * Remove shape from pane
     *
     * @param shape to remove
     */
    public void removeShape(IShape shape) {
        shapes.remove(shape);
        shape.removeShapeFromPane(this);
        eventManager.notifyListeners("removeShape");
    }

    /**
     * @return shapes iterator
     */
    public Iterator<IShape> getIterator() {
        return shapes.iterator();
    }

    @Override
    public Iterator<IShape> iterator() {
        return shapes.iterator();
    }

    /**
     * @return the command history
     */
    public CommandHistory getCommandHistory(){
        return this.commandHistory;
    }

    /**
     * @return selected shapes
     */
    public ArrayList<IShape> getSelection() {
        return this.selectionHandler.getSelectedShapes();
    }
}
