package drawing.core;

import drawing.commands.CommandHistory;
import drawing.observer.EventManager;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import drawing.status.StatusBar;
import drawing.status.ToolBar;

import java.util.ArrayList;

/**
 * Created by lewandowski on 20/12/2017.
 */
public class PaintApplication extends Application {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private BorderPane root;
    private DrawingPane drawingPane;
    private EventManager eventManager;

    @Override
    public void start(Stage primaryStage) {
        root = new BorderPane();
        Scene scene = new Scene(root, WIDTH, HEIGHT);

        root.getStylesheets().add(
                PaintApplication.class.getResource("../css/Paint.css").toExternalForm());

        eventManager = new EventManager();
        drawingPane = new DrawingPane(eventManager, new CommandHistory());
        drawingPane.getStyleClass().add("drawingPane");
        root.setCenter(drawingPane);

        this.buildToolbar();
        new StatusBar(root, eventManager).buildStatusBar(false);

        primaryStage.setTitle("Drawing");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Creates the toolbar
     */
    private void buildToolbar() {
        HBox hBox = new HBox();
        ToolBar toolBar = new ToolBar();

        ArrayList<Button> buttons = toolBar.build(drawingPane, eventManager);

        hBox.getChildren().addAll(buttons);
        hBox.setPadding(new Insets(5));
        hBox.setSpacing(5.0);
        hBox.getStyleClass().add("toolbar");

        root.setTop(hBox);
    }

    /**
     *
     * @return the drawing pane
     */
    public DrawingPane getDrawingPane() {
        return drawingPane;
    }

    /**
     * Launch app
     */
    public static void main(String[] args) {
        launch(args);
    }
}
