package drawing.status;

import drawing.core.DrawingPane;
import drawing.observer.EventManager;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import drawing.observer.FxObserverInterface;

public class StatusBar extends HBox implements FxObserverInterface {
    private Label label;

    private HBox hbox;
    private int shapeNumber = 0;
    private int shapeSelected = 0;
    private BorderPane root;

    public StatusBar(BorderPane root, EventManager eventManager) {
        this.root = root;
        eventManager.subscribe("addShape", this);
        eventManager.subscribe("cleanShape", this);
        eventManager.subscribe("removeShape", this);
        eventManager.subscribe("addSelectedShape", this);
        eventManager.subscribe("removeSelectedShape", this);
        eventManager.subscribe("cleanSelectedShape", this);
    }

    /**
     *
     * @return statusbar label
     */
    public Label getLabel() {
        return new Label("Too much shapes! (" + this.shapeNumber + ") and " + this.shapeSelected + " selected.");
    }

    /**
     *
     * @param refresh the status bar
     */
    public void buildStatusBar(boolean refresh) {
        Label label = getLabel();

        if (!refresh) {
            this.hbox = new HBox();
            hbox.getStyleClass().add("status-bar");
            hbox.getChildren().add(label);
        } else {
            hbox.getChildren().set(0, label);
        }

        root.setBottom(hbox);
    }

    @Override
    public void update(String event) {
        switch (event) {
            case "addShape":
                this.shapeNumber++;
                break;
            case "removeShape":
                this.shapeNumber--;
                break;
            case "cleanShape":
                this.shapeNumber = 0;
                break;
            case "addSelectedShape":
                this.shapeSelected++;
                break;
            case "removeSelectedShape":
                this.shapeSelected--;
                break;
            case "cleanSelectedShape":
                this.shapeSelected = 0;
                break;
        }

        this.buildStatusBar(true);
    }
}
