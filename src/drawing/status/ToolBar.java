package drawing.status;

import drawing.commands.*;
import drawing.core.DrawingPane;
import drawing.handlers.*;
import drawing.observer.EventManager;
import drawing.shapes.EllipseShape;
import drawing.shapes.RectangleShape;
import drawing.shapes.TriangleShape;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import java.util.ArrayList;

public class ToolBar {
    /**
     *
     * @param drawingPane to add buttons
     * @return all buttons
     */
    public ArrayList<Button> build(DrawingPane drawingPane, EventManager eventManager) {
        ArrayList<Button> buttons = new ArrayList<>();
        ButtonFactory buttonFactory = new ButtonFactory();

        Button clearButton = buttonFactory.createButton(ButtonFactory.CLEAR_BUTTON, ButtonFactory.TEXT_ONLY);
        clearButton.addEventFilter(ActionEvent.ACTION, new ButtonHandler(drawingPane, new ClearCommand(drawingPane, eventManager)));
        buttons.add(clearButton);

        Button rectangleButton = buttonFactory.createButton(ButtonFactory.RECTANGLE_BUTTON, ButtonFactory.TEXT_ONLY);
        rectangleButton.addEventFilter(ActionEvent.ACTION, new ShapeButtonHandler(drawingPane, new ShapeCommand(drawingPane, new RectangleShape(drawingPane))));
        buttons.add(rectangleButton);

        Button circleButton = buttonFactory.createButton(ButtonFactory.CIRCLE_BUTTON, ButtonFactory.TEXT_ONLY);
        circleButton.addEventFilter(ActionEvent.ACTION, new ShapeButtonHandler(drawingPane, new ShapeCommand(drawingPane, new EllipseShape(drawingPane))));
        buttons.add(circleButton);

        Button triangleButton = buttonFactory.createButton(ButtonFactory.TRIANGLE_BUTTON, ButtonFactory.TEXT_ONLY);
        triangleButton.addEventFilter(ActionEvent.ACTION, new ShapeButtonHandler(drawingPane, new ShapeCommand(drawingPane, new TriangleShape(drawingPane))));
        buttons.add(triangleButton);

        Button removeButton = buttonFactory.createButton(ButtonFactory.REMOVE_BUTTON, ButtonFactory.TEXT_ONLY);
        removeButton.addEventFilter(ActionEvent.ACTION, new ButtonHandler(drawingPane, new RemoveCommand(drawingPane)));
        buttons.add(removeButton);

        Button groupButton = buttonFactory.createButton(ButtonFactory.GROUP_BUTTON, ButtonFactory.TEXT_ONLY);
        groupButton.addEventFilter(ActionEvent.ACTION, new ButtonHandler(drawingPane, new GroupCommand(drawingPane)));
        buttons.add(groupButton);

        Button ungroupButton = buttonFactory.createButton(ButtonFactory.UNGROUP_BUTTON, ButtonFactory.TEXT_ONLY);
        ungroupButton.addEventFilter(ActionEvent.ACTION, new ButtonHandler(drawingPane, new UngroupCommand(drawingPane)));
        buttons.add(ungroupButton);

        Button undoButton = buttonFactory.createButton(ButtonFactory.UNDO_BUTTON, ButtonFactory.TEXT_ONLY);
        undoButton.addEventFilter(ActionEvent.ACTION, new UndoButtonHandler(drawingPane));
        buttons.add(undoButton);

        Button redoButton = buttonFactory.createButton(ButtonFactory.REDO_BUTTON, ButtonFactory.TEXT_ONLY);
        redoButton.addEventFilter(ActionEvent.ACTION, new RedoButtonHandler(drawingPane));
        buttons.add(redoButton);

        return buttons;
    }
}
