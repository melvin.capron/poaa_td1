package drawing.status;

import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ButtonFactory {
    public static final String CLEAR_BUTTON = "Clear";
    public static final String RECTANGLE_BUTTON = "Rectangle";
    public static final String CIRCLE_BUTTON = "Circle";
    public static final String TRIANGLE_BUTTON = "Triangle";
    public static final String REMOVE_BUTTON = "Remove";
    public static final String GROUP_BUTTON = "Group";
    public static final String UNGROUP_BUTTON = "Ungroup";
    public static final String UNDO_BUTTON = "Undo";
    public static final String REDO_BUTTON = "Redo";

    public static final int ICONS_ONLY = 0;
    public static final int TEXT_ONLY = 1;

    /**
     *
     * @param buttonName to create
     * @param style Icons or Text
     * @return button
     */
    public Button createButton(String buttonName, int style) {
        Button button = null;

        switch (buttonName) {
            case CLEAR_BUTTON:
                button = buildButton(style, buttonName, "clear");
                button.setTooltip(new Tooltip(CLEAR_BUTTON));
                break;
            case RECTANGLE_BUTTON:
                button = buildButton(style, buttonName, "rectangle");
                button.setTooltip(new Tooltip(RECTANGLE_BUTTON));
                break;
            case CIRCLE_BUTTON:
                button = buildButton(style, buttonName, "square");
                button.setTooltip(new Tooltip(CIRCLE_BUTTON));
                break;
            case TRIANGLE_BUTTON:
                button = buildButton(style, buttonName, "triangle");
                button.setTooltip(new Tooltip(TRIANGLE_BUTTON));
                break;
            case REMOVE_BUTTON:
                button = buildButton(style, buttonName, "delete");
                button.setTooltip(new Tooltip(REMOVE_BUTTON));
                break;
            case GROUP_BUTTON:
                button = buildButton(style, buttonName, "group");
                button.setTooltip(new Tooltip(GROUP_BUTTON));
                break;
            case UNGROUP_BUTTON:
                button = buildButton(style, buttonName, "ungroup");
                button.setTooltip(new Tooltip(UNGROUP_BUTTON));
                break;
            case UNDO_BUTTON:
                button = buildButton(style, buttonName, "undo");
                button.setTooltip(new Tooltip(UNDO_BUTTON));
                break;
            case REDO_BUTTON:
                button = buildButton(style, buttonName, "redo");
                button.setTooltip(new Tooltip(REDO_BUTTON));
                break;
        }

        return button;
    }

    private Button buildButton(int style, String text, String image) {
        if (style == ICONS_ONLY) {
            return new Button(null, this.addIcon(image));
        }

        //default behavior
        return new Button(text);
    }

    private ImageView addIcon(String imageName) {
        Image image = new Image(this.getClass().getResourceAsStream("/drawing/images/" + imageName + ".png"));
        ImageView imageView = new ImageView(image);

        imageView.setFitHeight(10);
        imageView.setFitWidth(10);
        return imageView;
    }
}
