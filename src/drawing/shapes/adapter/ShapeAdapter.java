package drawing.shapes.adapter;

import drawing.shapes.interfaces.IShape;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

public class ShapeAdapter implements IShape {
    Shape shape;

    public ShapeAdapter(Shape shape) {
        this.shape = shape;
    }

    @Override
    public void offset(double x, double y) {
        this.shape.setTranslateX(this.shape.getTranslateX() + x);
        this.shape.setTranslateY(this.shape.getTranslateY() + y);
    }

    @Override
    public double getX() {
        return this.shape.getTranslateX();
    }

    @Override
    public double getY() {
        return this.shape.getTranslateY();
    }

    @Override
    public void addShapeToPane(Pane pane) {
        pane.getChildren().add(shape);
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        pane.getChildren().remove(shape);
    }

    @Override
    public boolean isSelected() {
        return shape.getStyleClass().contains("selected");
    }

    @Override
    public void setSelected(boolean selected) {
        if (selected) {
            shape.getStyleClass().add("selected");
        } else {
            shape.getStyleClass().remove("selected");
        }
    }

    @Override
    public boolean isOn(double x, double y) {
        return shape.getBoundsInParent().contains(x, y);
    }
}
