package drawing.shapes.adapter;

import drawing.shapes.interfaces.IShape;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

import java.util.ArrayList;

public class ShapeGroup implements IShape {
    private final ArrayList<IShape> shapes = new ArrayList<>();

    @Override
    public void offset(double x, double y) {
        for (IShape shape : shapes) {
            shape.offset(x, y);
        }
    }

    @Override
    public double getX() {
        for (IShape shape : shapes) {
            return shape.getX();
        }

        return 0;
    }

    @Override
    public double getY() {
        for (IShape shape : shapes) {
            return shape.getY();
        }

        return 0;
    }

    @Override
    public void addShapeToPane(Pane pane) {
        for (IShape shape : shapes) {
            shape.addShapeToPane(pane);
        }
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        for (IShape shape : shapes) {
            shape.removeShapeFromPane(pane);
        }
    }

    @Override
    public boolean isSelected() {
        for (IShape shape : shapes) {
            return shape.isSelected();
        }

        return false;
    }

    @Override
    public void setSelected(boolean selected) {
        for (IShape shape : shapes) {
            shape.setSelected(selected);
        }
    }

    @Override
    public boolean isOn(double x, double y) {
        for (IShape shape : shapes) {
            if (shape.isOn(x, y)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param shape to add to the group
     */
    public void add(IShape shape) {
        shapes.add(shape);
    }

    /**
     * @return all shapes
     */
    public ArrayList<IShape> getShapes() {
        return this.shapes;
    }
}
