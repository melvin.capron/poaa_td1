package drawing.shapes.interfaces;

import javafx.scene.layout.Pane;

import java.util.ArrayList;

public interface IShape {
    boolean isSelected();
    void setSelected(boolean selected);
    boolean isOn(double x, double y);
    void offset(double x, double y);
    double getX();
    double getY();
    void addShapeToPane(Pane pane);
    void removeShapeFromPane(Pane pane);
}
