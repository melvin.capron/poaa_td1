package drawing.shapes.interfaces;

public interface ShapeCreator {
    public abstract IShape createShape();
}
