package drawing.shapes;

import drawing.core.DrawingPane;
import drawing.handlers.ShapeButtonHandler;
import drawing.shapes.adapter.ShapeAdapter;
import drawing.shapes.interfaces.IShape;
import javafx.scene.shape.Polygon;

public class TriangleShape extends AbstractShape {

    public TriangleShape(DrawingPane drawingPane) {
        super(drawingPane);
    }

    /*
        This triangle is not working properly, there is a white gap that can be "dragged"
        the triangle is much bigger that it seems
    */
    @Override
    public IShape createShape() {
        double x = Math.min(originX, destinationX);
        Polygon triangle = new Polygon();
        triangle.getPoints().addAll(
                Math.abs(originX - x / 2), originY,
                originX, destinationY,
                destinationX, destinationY);

        return new ShapeAdapter(triangle);
    }
}
