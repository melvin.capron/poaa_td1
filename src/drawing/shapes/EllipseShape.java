package drawing.shapes;

import drawing.core.DrawingPane;
import drawing.shapes.adapter.ShapeAdapter;
import drawing.shapes.interfaces.IShape;

public class EllipseShape extends AbstractShape {

    public EllipseShape(DrawingPane drawingPane) {
        super(drawingPane);
    }

    @Override
    public IShape createShape() {
        double x = Math.min(originX, destinationX);
        double y = Math.min(originY, destinationY);

        double width = Math.abs(destinationX - originX);
        double height = Math.abs(destinationY - originY);

        javafx.scene.shape.Ellipse ellipse = new javafx.scene.shape.Ellipse(x + width / 2, y + height / 2, width / 2, height / 2);
        ellipse.getStyleClass().add("ellipse");

        return new ShapeAdapter(ellipse);
    }
}
