package drawing.shapes;

import drawing.core.DrawingPane;
import drawing.shapes.adapter.ShapeAdapter;
import drawing.shapes.interfaces.IShape;

public class RectangleShape extends AbstractShape {

    public RectangleShape(DrawingPane drawingPane) {
        super(drawingPane);
    }

    @Override
    public IShape createShape() {
        double x = Math.min(originX, destinationX);
        double y = Math.min(originY, destinationY);

        double width = Math.abs(destinationX - originX);
        double height = Math.abs(destinationY - originY);

        javafx.scene.shape.Rectangle rectangle = new javafx.scene.shape.Rectangle(x, y, width, height);
        rectangle.getStyleClass().remove("rectangle");

        return new ShapeAdapter(rectangle);
    }


}
