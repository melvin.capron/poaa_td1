package drawing.shapes;

import drawing.core.DrawingPane;
import drawing.shapes.interfaces.IShape;
import drawing.shapes.interfaces.ShapeCreator;

public abstract class AbstractShape implements ShapeCreator {
    double originX;
    double originY;
    double destinationX;
    double destinationY;
    private DrawingPane drawingPane;

    public AbstractShape(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
    }

    public void populateCoords(double originX, double originY, double destinationX, double destinationY) {
        this.originX = originX;
        this.originY = originY;
        this.destinationX = destinationX;
        this.destinationY = destinationY;
    }

    public abstract IShape createShape();
}
