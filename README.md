**TD POOA 1**

- Forme géométrique utilisée : Polygon
- Classes ajoutée : TriangleButtonHandler
- 1 Test Unitaire ajouté

- **drawingPane.addShape** :
    - Si on le remplace, on se rend compte que la forme est ajoutée uniquement à la liste et non plus au pane comme elle l'était avant. De fait, on perd la mise à jour du visuel.
    - Implémentation de la classe Iterator par DrawingPane pour mettre une méthode `iterator()` qui va permettre uniquement la lecture des formes
    
**TP 4**
-  Retour visuel :
    *  Il suffit de rajouter les events au notifier afin que le SelectionHandler soit au courant

- Factory :
    * createShape est déjà un FactoryMethod puisqu'il renvoie une form selon le paramètre qui est passé